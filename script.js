const inputCalc = document.querySelector('.account'),
      resultCalc = document.querySelector('.result'),
      alertSafe = document.querySelector('.alertSafe');
var i0 = '';

function input(i) {
    if ((i == '+' || i == '-' || i == '*' || i == '/' || i == '.') &&
        (i0 == '+' || i0 == '-' || i0 == '*' || i0 == '/' || i0 == '.')) {
        backspace();
    }
    if ((i == '+' || i == '-' || i == '*' || i == '/' || i == '.') && inputCalc.value == '') {
        inputCalc.value = '0'
    }
    inputCalc.value = inputCalc.value + i;
    i0 = i;
}

function result() {
    var ev = eval(inputCalc.value);

    if (ev > Number.MAX_VALUE || ev < Number.MIN_VALUE) {
        alert('Введенное число выходит за пределы возможных значений!');
    } else {
        if(ev == undefined) {
            inputCalc.value = '';
            resultCalc.innerHTML = '0';
        } else if(ev == Infinity || ev == -Infinity || isNaN(ev)) {
            alert('Делить на 0 нельзя!');
        } else {
            if (ev == 0) {
                inputCalc.value = '';
            } else {
                inputCalc.value = ev;
                if (Number.isInteger(ev) && !Number.isSafeInteger(ev)) {
                    alertSafe.innerHTML = 'Выход за пределы безопасного диапазона';
                } else {
                    alertSafe.innerHTML = '';
                }
            }  
            if (String(ev).length > 11) {
                resultCalc.innerHTML = ev.toExponential(4);
            } else {
                resultCalc.innerHTML = ev;
            }
        }
    }
}
function backspace() {
    inputCalc.value = inputCalc.value.slice(0, -1);
}

function ce() {
    if (inputCalc.value == '') {
        resultCalc.innerHTML = '0';
    } else {
        inputCalc.value = '';
    }
}
